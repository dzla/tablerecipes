export PlotMulti, plottable, plottable!, plottable2d
export StatHeatmap
export plotmulti, plotmulti!

@userplot PlotMulti 

const COL_FIXED_COLOR = :PLOTTABLE_color_of_group
const COL_FIXED_LINESTYLE = :PLOTTABLE_linestyle_of_group

# Plot Recipe
@recipe function f(::Type{Val{:plotmulti}}, x, y, z; 	# Need z, even if 2d!
				   linestyle_force = :auto,
				   color_force = :auto,
				   y_statistic = median, 
				   yerror_statistic = nothing, 
				   yribbon_statistic = nothing,
				   label = repeat([""], length(x)),
				   showstats = true,
				   display_reduced_table = false,
				   verbose = false)

	df = DataFrame([x, y, label], [:x,:y,:l])
	tsz = size(df,1)
	numdiff = length(unique(x))
	@debug "Reducing data, entry is a $(size(df)) DataFrame."
	@assert all(.~isnan.(x)) "Got NaN values for the x-variable used for plotting."
	@assert all(.~isnan.(y)) "Got NaN values for the y-variable used for plotting."
	# Compute the "reduced" data
	r = combine(groupby(df, [:x]), 
				:y => y_statistic => :staty, 
				:y => (yerror_statistic isa Nothing ? std : yerror_statistic) => :stdy, 
				:y => (yribbon_statistic isa Nothing ? std : yribbon_statistic) => :ribbony, 
				:y => length => :Nforx,
				:l => (x -> string("$(x[1])", showstats ? " ($(numdiff)/$(floor(tsz))/$(floor(tsz/numdiff)))" : "")) => :l )
	@info "Using $(sum(r[:,:Nforx])) values for $(size(r,1)) points (≈$(ceil(sum(r[:,:Nforx])/size(r,1))) vals/pt)."
	sort!(r, :x)
	verbose && for i in 1:size(r,1)
		@debug "Using $(r[i,:Nforx]) values for x=$(r[i,:x])."
	end
	display_reduced_table && display(r)
	rx = r[:,:x]
	ry = r[:,:staty]
	# println("plotting $rx againg $ry")
	# if linestyle_force ≠ nothing
		@series begin
			# seriescolor := :blue
			label := r[1, :l]
			seriestype := :path
			x := rx
			y := ry
			primary := true
			# linestyle := linestyle_force[1]
			linestyle := ((linestyle_force ≠ :auto) ? linestyle_force[1] : :auto)
			seriescolor := ((color_force ≠ :auto) ? color_force[1] : :auto)
			()
		end
	# else
		# @series begin
			# label := r[1, :l]
			# seriestype := :path
			# x := rx
			# y := ry
			# primary := true
			# ()
		# end
	# end
	if ~(yribbon_statistic isa Nothing)
		rib = r[:,:ribbony]
		tokeep = .~isnan.(rib)
		@series begin
			seriestype := :path
			x := rx[tokeep]
			y := ry[tokeep]
			fillrange := (ry[tokeep] - rib[tokeep], ry[tokeep] + rib[tokeep])
			primary := false
			linestyle := ((linestyle_force ≠ :auto) ? linestyle_force[1] : :auto)
			seriescolor := ((color_force ≠ :auto) ? color_force[1] : :auto)
			()
		end
	elseif ~(yerror_statistic isa Nothing)
		@series begin
			seriestype := :yerror
			x := rx
			y := ry
			yerror := r[:, :stdy]
			primary := false
			seriescolor := ((color_force ≠ :auto) ? color_force[1] : :auto)
			()
		end
	end
	nothing
end

# User Recipe
@recipe function f(h::PlotMulti)
	@assert length(h.args) == 2
	seriestype := :plotmulti
	h.args
end

function assertin(s, n)
	@assert (string(s) in n) "String $s is not a valid column."
end

"""
find_color(g, assign_color)
Returns a value of the dictionary `assign_color` whose key is matches `g` (according to the function `occursin`, in particular keys of `assign_color` can be `String` or `Regex`).
"""
function find_color(g, assign_color)
	ks = keys(assign_color)
	# idx = findlast(occursin.(ks, lowercase(g)))
	match_g = (p->occursin(p[1], lowercase(g)))
	matching_values = values(filter(match_g, assign_color))
	if ~isempty(matching_values)
		if length(matching_values) > 1
			@warn "Multiple matching keys values for $g (matching: $(keys(matching_values.dict)))."
		end
		first(matching_values)
	else
		:auto
	end
end

function plottable!(df, xs, ys, args...; linestyles = [:solid, :dash, :dotdotdash, :dot, :dotdash], linestyle_group_on = nothing, group = :label, label = :label, kwargs...)
	df = copy(df) # To allow adding a column
	n = names(df)
	assertin(xs, n)
	assertin(ys, n)
	if typeof(group) <: Symbol
		assertin(group, n)
	elseif typeof(group) <: Array{Symbol}
		@assert all([string(gs) in n for gs in group])
	end
	if linestyle_group_on ≠ nothing
		ugr = unique(df[:, linestyle_group_on])
		ls_idx(x) = (x-1) % length(ugr) + 1
		df[!, COL_FIXED_LINESTYLE] = (x -> linestyles[ls_idx(findfirst(x .== ugr))]).(df[:, linestyle_group_on])
	else
		df[!, COL_FIXED_LINESTYLE] .= :auto
	end
	@df df plotmulti!(cols(xs), cols(ys), args...; 
					  linestyle_force = cols(COL_FIXED_LINESTYLE),
					  group = cols(group),
					  label = cols(label), 
					  kwargs...)
end

function plottable(df, xs, ys, args...; 
					linestyles = [:solid, :dash, :dotdotdash, :dot, :dotdash],
					linestyle_group_on = nothing, 
					assign_color = nothing, 
					group = :label,
					label = :label,
					kwargs...)
	df = copy(df) # To allow adding a column
	n = names(df)
	assertin(xs, n)
	assertin(ys, n)
	if typeof(group) <: Symbol
		assertin(group, n)
	elseif typeof(group) <: Array{Symbol}
		@assert all([(string(gs) in n) for gs in group])
	end
	if linestyle_group_on ≠ nothing
		ugr = unique(df[:, linestyle_group_on])
		ls_idx(x) = (x-1) % length(ugr) + 1
		df[!, COL_FIXED_LINESTYLE] = (x -> linestyles[ls_idx(findfirst(x .== ugr))]).(df[:, linestyle_group_on])
	else
		df[!, COL_FIXED_LINESTYLE] .= :auto
	end
	@debug "assign_color" assign_color
	if assign_color ≠ nothing
		df[!, COL_FIXED_COLOR] .= find_color.(df[:,label], Ref(assign_color))
	else
		df[!, COL_FIXED_COLOR] .= :auto
	end
	@df df plotmulti(cols(xs), cols(ys), args...;
					 linestyle_force = cols(COL_FIXED_LINESTYLE),
					 color_force = cols(COL_FIXED_COLOR), 
					 group = cols(group),
					 label = cols(label), kwargs...)
	# plotmulti(df[:,xs], df[:,ys], args...; 
			  # linestyle_force = df[:,COL_FIXED_LINESTYLE],
			  # color_force = df[:,COL_FIXED_COLOR],
			  # group = df[:,group], 
			  # label = df[:,label],
			  # kwargs...)
end

#######################################################################
#                               Tables                                #
#######################################################################

@userplot StatHeatmap 

# User Recipe
@recipe function f(h::StatHeatmap)
	@assert length(h.args) == 3
	seriestype := :statheatmap
	h.args
end

# Plot Recipe
@recipe function f(::Type{Val{:statheatmap}}, x, y, z; 
				   z_statistic = median,
				   zscale = :lin )

	df = DataFrame([x, y, z], [:x,:y,:z])
	# r = by(df, [:x, :y], 
				   # statz = :z => z_statistic, 
				   # nsamples = :z => length )
	r = combine(groupby(df, [:x,:y]), 
				:z => z_statistic => :statz,
				:z => length => :nsamples)
	xu = sort(unique(x))
	yu = sort(unique(y))
	zstats = NaN*ones(length(yu), length(xu))
	for i = 1:length(xu), j = 1:length(yu)
		t = r[isapprox.(r.x, xu[i]) .& isapprox.(r.y, yu[j]), :statz]
		if ~isempty(t)
			zstats[j, i] = t[1]
		end
	end
	# if zscale == :log10
		# zstats .= log10.(zstats)
	# end

	seriestype := :heatmap
	x := xu
	y := yu
	z := Surface(zstats)
	primary := true
	()
end

function plottable2d(df, xs, ys, zs, args...; kwargs...)
	n = names(df)
	assertin(xs, n)
	assertin(ys, n)
	assertin(zs, n)
	@df df statheatmap(cols(xs), cols(ys), cols(zs), args...; kwargs...)
end

