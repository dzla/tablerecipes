"""
	shared_keys, shared_vals, other_keys = split_shared(df)

	Separates keys (columns names) of `df` for which there is only one unique value in `df` (`shared_keys`, in which case these unique values are stored in a dictionary `shared_vals`) from others (`other_keys`).

"""
function split_shared(df)
	@assert size(df, 1) > 1
	# kall = names(df)
	kall = Symbol.(names(df))
	shared_vals = Dict()
	shared_keys = Vector{Symbol}()
	other_keys = Vector{Symbol}()
	for k in kall
		if length(unique(df[:,k])) == 1
			push!(shared_keys, k)
			shared_vals[k] = df[1, k]
		else
			push!(other_keys, k)
		end
	end
	(shared_keys, shared_vals, other_keys)
end

function get_assert_unique(df, key)
	val = df[1, key]
	@assert all(df[.~ismissing.(df[:,key]), key] .== val) "Key $key has not a unique value in the table."
	return val
end

#####################
#  Title & Legends  #
#####################

function maketitle(d; exclude = [:label, :usefasttransforms, :observations, :α, :Fourier_renormalization, :fname_R, :m, :mean_intra_var, :kernel_var_choice],
				   	  exclude_extra =[],
					  hidekey = [:algorithm, :decoder, :algorithm, :dataset, :fname_C, :fname_X, :radialdist],
				   	  hidekey_extra =[],
					  replace = Dict(:CommitHash => :Commit, :LearningActivation => :LrnNL, 
					  				 :SketchingActivation => :SkNL),
					  minlinewidth = 0)
	exclude = vcat(exclude, exclude_extra)
	hidekey = vcat(hidekey, hidekey_extra)
	dclean = copy(d)
	for k in exclude
		delete!(dclean, k)
	end
	krpl = keys(replace)
	res = join([(k in hidekey ? "$v" : (k in krpl ? "$(replace[k])=$v" : "$k=$v")) for (k,v) in dclean], ", ")
	if minlinewidth > 0
		ar = split(res, ",")
		i = 1
		while i ≤ length(ar)
			while length(ar[i]) < minlinewidth && i < length(ar)
				ar = vcat(ar[1:i-1], ["$(ar[i]),$(ar[i+1])"], ar[i+2:end])
			end
			i += 1
		end
		res = join(ar, ",\n")
	end
	return res
end

"""
$(SIGNATURES)
Assigns groups to the entries based on the value of the column `sb`.
The ordering of the table is taken into account to create the groups, hence the order in which groups will be processed might differ from the one obtained when groupping directly on `sb`.
"""
function create_groups!(df, sb=:label)
	all_labels = reshape(sort(unique(df[:,sb])), 1,:)
	eq_tab = (df.label .== all_labels)
	df[!,:group] = findfirst.(eachrow(eq_tab))
	# sort!(df, :group)
end
