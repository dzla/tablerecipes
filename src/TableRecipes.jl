module TableRecipes

using Requires
using DataFrames
using Statistics
using DocStringExtensions

export split_shared, maketitle
export get_assert_unique
export create_groups!

include("utils.jl")

function __init__()
	@require Plots="91a5bcdd-55d7-5caf-9e0b-520d859cae80" begin
		using .Plots
		@require StatsPlots = "f3b207a7-027a-5e70-b257-86293d7955fd" begin
			using .StatsPlots
			include("plots.jl")
		end
	end
	@require Makie="ee78f7c6-11fb-53f2-987a-cfe4a2b5a57a" begin
		using .Makie
		include("makie.jl")
	end
end

end # module

