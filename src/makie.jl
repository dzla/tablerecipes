function plottable2d(df, xs, ys, zs, args...; 
					 # group = :group, 
					 label = :Label, 
					 z_statistic = median, 
					 kwargs...)
	n = names(df)
	@assert xs in n
	@assert ys in n
	@assert zs in n
	# if typeof(group) <: Symbol
		# @assert group in n
	# elseif typeof(group) <: Array{Symbol}
		# @assert all([(gs in n) for gs in group])
	# end
	# One plot per group
	# by(df, group) do dfss
		# For each group :wqa
		xu = sort(unique(df[:, xs]))
		yu = sort(unique(df[:, ys]))
		zstats = Matrix{Float64}(undef, length(xu), length(yu))
		for i = 1:length(xu)
			for j = 1:length(yu)
				zstats[i, j] = z_statistic(df[(df[xs] .== xu[i]) .& (df[ys] .== yu[j]), zs])
			end
		end
		Makie.heatmap(xu, yu, zstats, 
					  interpolate=true)
		# @df df plotmulti(cols(xs), cols(ys), args...; group = cols(group), label = cols(label), kwargs...)
	# end
end

# function plottable!(df, xs, ys, args...; 
					# group = (), 
					# label = :Label, 
					# kwargs...)

	# n = names(df)
	# @assert xs in n
	# @assert ys in n
	# if typeof(group) <: Symbol
		# @assert group in n
	# elseif typeof(group) <: Array{Symbol}
		# @assert all([(gs in n) for gs in group])
	# end

	# @df df plotmulti!(cols(xs), cols(ys), args...; group = cols(group), label = cols(label), kwargs...)
# end

# @userplot PlotMulti 
# @recipe(PlotStatX, x, y) do scene
    # Theme()
# end

# # convert_arguments(df::DataFrame, xs) = ()
# function plot!(plot::PlotStatX)
	# df = DataFrame([plot[:x], plot[:y]], [:x,:y])
	# tsz = size(df,1)
	# numdiff = length(unique(plot[:x]))
	# # Compute the "reduced" data
	# r = by(df, :x, 
			   # staty = :y => y_statistic,
			# Nforx = :y => length,
		   # )
	# # stdy = :y => (yerror_statistic isa Nothing ? std : yerror_statistic), 
	# # ribbony = :y => (yribbon_statistic isa Nothing ? std : yribbon_statistic), 
	# # l = :l => x -> string("$(x[1])", showstats ? " ($(floor(tsz))/$(floor(tsz/numdiff)))" : "") 
	# println("Using $(sum(r[:Nforx])) values for $(size(r,1)) points (≈$(ceil(sum(r[:Nforx])/size(r,1))) vals/pt).")
	# sort!(r, :x)
	# rx = r[:x]
	# ry = r[:staty]

	# println(typeof(plot))
	# println(plot[:dir])
	# return plot
# end


